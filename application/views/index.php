<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body class="bg-light pt-5">

<section>
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-12 col-md-8 col-lg-8 col-xl-6">
          <div class="row">
            <div class="col text-center">
              <h1>Pendaftaran</h1>
            </div>
          </div>
          <form action="<?= base_url('index.php/welcome/simpan'); ?>" method="post">
          <div class="error-validation text-danger text-center">
          <?php echo validation_errors(); ?>
          </div>
            <div class="row align-items-center">
              <div class="col mt-4">
                <input name="username" type="text" class="form-control" placeholder="Masukkan Username" value="<?php echo set_value('username'); ?>">
              </div>
            </div>
            <div class="row align-items-center mt-4">
              <div class="col">
                <input name="email" type="email" class="form-control" placeholder="Masukkan Email" value="<?php echo set_value('email'); ?>">
              </div>
            </div>
            <div class="row align-items-center mt-4">
              <div class="col">
                <input name="password" type="password" class="form-control" placeholder="Masukkan Password" value="<?php echo set_value('password'); ?>">
              </div>
              <div class="col">
                <input name="conf_password" type="password" class="form-control" placeholder="Ulangi Password" value="<?php echo set_value('conf_password'); ?>">
              </div>
            </div>
            <div class="row mt-2">
              <div class="col">
                <button class="btn btn-primary mt-4" type="submit">Daftar</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>