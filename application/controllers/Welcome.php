<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('index');
	}

	public function simpan()
	{
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');

		// Validation begin
		$this->form_validation->set_rules('username', 'Username', 'required',
			array('required' => 'Username harus diisi.')
		);
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email', 
			array(
				'required' => 'Email harus diisi.',
				'valid_email' => 'Email tidak valid.',
			)
		);
		$this->form_validation->set_rules('password', 'Password', 'required',
			array('required' => 'Password harus diisi.')
		);
		$this->form_validation->set_rules('conf_password', 'Password Confirmation', 'required|matches[password]',
			array(
				'required' => 'Konfirmasi Password harus diisi.',
				'matches' => 'Password tidak sama.',
			)
		);

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('index');
		}
		else
		{
			$this->load->view('sukses');
		}
	}

	public function sukses()
	{
		$this->load->view('sukses');
	}
}
